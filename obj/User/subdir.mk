################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../User/ch32v30x_it.c \
../User/core_list_join.c \
../User/core_main.c \
../User/core_matrix.c \
../User/core_state.c \
../User/core_util.c \
../User/system_ch32v30x.c 

OBJS += \
./User/ch32v30x_it.o \
./User/core_list_join.o \
./User/core_main.o \
./User/core_matrix.o \
./User/core_state.o \
./User/core_util.o \
./User/system_ch32v30x.o 

C_DEPS += \
./User/ch32v30x_it.d \
./User/core_list_join.d \
./User/core_main.d \
./User/core_matrix.d \
./User/core_state.d \
./User/core_util.d \
./User/system_ch32v30x.d 


# Each subdirectory must supply rules for building sources it contributes
User/%.o: ../User/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU RISC-V Cross C Compiler'
	riscv-none-embed-gcc -march=rv32imafc -mabi=ilp32f -msmall-data-limit=8 -mno-save-restore -O3 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Wunused -Wuninitialized  -g -I"/home/elec/workspace/MRS/CH32V307VCT6/Debug" -I"/home/elec/workspace/MRS/CH32V307VCT6/User/barebones" -I"/home/elec/workspace/MRS/CH32V307VCT6/Core" -I"/home/elec/workspace/MRS/CH32V307VCT6/User" -I"/home/elec/workspace/MRS/CH32V307VCT6/Peripheral/inc" -std=gnu99 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


