# ch32v307_coreMark

#### 介绍
沁恒CH32V307VCT6 芯片 CoreMark 1.0 benchmark。

#### CoreMark 结果
CoreMark 1.0 : 380.66 @144MHz, 2.64/MHz.

2K performance run parameters for coremark.  
CoreMark Size    : 666  
Total ticks      : 5254  
Total time (secs): 10.508000  
Iterations/Sec   : 380.662352  
Iterations       : 4000  
Compiler version : GCC8.2.0  
Compiler flags   : -o3  
Memory location  : STACK  
seedcrc          : 0xe9f5  
[0]crclist       : 0xe714  
[0]crcmatrix     : 0x1fd7  
[0]crcstate      : 0x8e3a  
[0]crcfinal      : 0x65c5  
Correct operation validated. See README.md for run and reporting rules.  
CoreMark 1.0 : 380.662352 / GCC8.2.0 -o3 / STACK  

#### 使用说明

MounRiver Linux 社区版 V1.00


